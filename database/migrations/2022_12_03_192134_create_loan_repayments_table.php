<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_repayments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('loan_id')->index()->constrained('loan_applications');
            $table->date('emi_date');
            $table->double('emi_amount', 8, 2)->index();
            $table->double('paid_amount', 8, 2)->nullable()->index();
            $table->timestamp('paid_at')->nullable();
            $table->enum('payment_status', ['pending', 'paid', 'na'])->default('pending')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_repayment');
    }
};
