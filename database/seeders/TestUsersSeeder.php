<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TestUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin User
        User::create([
            'first_name' => 'Admin',
            'last_name' => 'Example',
            'email' => 'admin@example.com',
            'is_admin' => 1,
            'password' => Hash::make('password')
        ]);

        //Customer User
        User::create([
            'first_name' => 'Customer',
            'last_name' => 'Example',
            'email' => 'customer@example.com',
            'is_admin' => 0,
            'password' => Hash::make('password')
        ]);
    }
}
