<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\LoanApplication>
 */
class LoanApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'loan_unique_id' => 'AL'.uniqid(),
            'loan_term' => fake()->numberBetween(1, 24),
            'loan_status' => fake()->randomElement(['pending', 'approved', 'rejected', 'paid', 'cancelled']),
            'total_amount' => fake()->numberBetween(1000, 100000),
            'user_id' => User::where('is_admin', 0)->inRandomOrder()->first()->id
        ];
    }
}
