# Mini-Aspire Loan API

A Laravel application that helps user to apply for loan for a specific term period and
then repay the part of amount every week once admin approves the loan application.

This api consists of three different modules,

### Authentication

Contains registration, login and logout functionality for users (Admin and Customers).
It uses [Laravel Sanctum](https://laravel.com/docs/9.x/sanctum) for token based authentication
system. When user registers or perform login operation, api will return auth token in the response
that can be used to authorize protected APIs later.

### Loan application

Contains logic to create, approve/reject the loan application.
Loan applications are created by customer but can be only approved by admins.
This module also contains functionality to get approved/pending loan applications.
In theory, via loan application user can inform admin that he wants to apply for a
loan of X amount for X period. Then admin needs to approve that application.

### Loan repayments

Contains logic to get pending/paid loan repayments and repay a specific repayment.
Loan repayment records are created once admin approves the loan application.
Once loan repayment records are created, user can fetch the list of repayments for a
specific loan application and then repay a particular repayment.
Currently, api assumes a weekly repayment frequency.


## Project structure

This application follows a default directory structure of Laravel.
Below are the directories that contains the main logic,

`api/Models`

Contains models for user, loan application and loan repayment.

`api/Http/Controllers`

Contains all controllers to handle routes functionalities.

`database/factories`

Contains factories that can be used to generate mock data for models.

`database/migrations`

Contains migrations for database schema.

`database/seeders`

Contains database seeder for models that can be used to generate mock data to init the api.

`app/Http/Resources`

Contains JSON resources specifically for models.

`app/Http/Requests`

Contains rules to validate route requests that can be injected in controller methods.

`app/Events`

Contains events that can be called after/before a series of actions.

`routes`

Contains all the API routes and controller methods mapping.

`tests/Unit`

Contains all the unit tests.

`tests/Feature`

Contains all the feature tests.

`app/Actions`

Contains small and isolated actions that can be used separately or in combination to perform a series of operations.

`app/Repository`

Handles the operations done to the database specifically, so that Code Redundancy can be reduced

## Installation

### Prerequisites

-  PHP >= 8.0
-  MySQL >= 8.0
-  Mcrypt PHP Extension
-  OpenSSL PHP Extension
-  Mbstring PHP Extension
-  Tokenizer PHP Extension
-  Composer

You can execute below file to set up project.
It will also test the test cases before serving the project

```
sh ./bin/setup.sh
```

## Response structure

#### Success response

```json
{
    "success": true,
    "message": "Admin Created.",
    "meta": {
        "timestamp": "2022-12-07T18:00:26.644135Z",
        "version": "1.0"
    },
    "data": {
        "first_name": "Customer",
        "last_name": "Example",
        "email": "admin@gmail.com",
        "is_admin": 1,
        "updated_at": "2022-12-07T18:00:26.000000Z",
        "created_at": "2022-12-07T18:00:26.000000Z",
        "id": 7,
        "token": "3|PIkEho3Jd33VY3Tpks9vqsXvYRLLwkaGJxz9nfX1"
    }
}
```

#### Error response

```json
{
    "success": false,
    "message": "User not authenticated.",
    "meta": {
        "timestamp": "2022-12-07T21:33:23.458319Z",
        "version": "1.0"
    }
}
```

#### Validation error response

```json
{
    "message": "The email must be a valid email address.",
    "errors": {
        "email": [
            "The email must be a valid email address."
        ]
    }
}
```

## Postman collection

[Mini Aspire APIs.postman_collection.json](https://gitlab.com/manish011994/mini-aspire/-/blob/main/Mini%20Aspire%20APIs.postman_collection.json)
you can find the postman collection for all the APIs that can help customer and admin to interact with the application.
Once you import the collection, You can call the different APIs stored in the directories

## Test users

You can use below users to test the APIs.

#### Admin user

Email: `admin@example.com`
Password: `password`

#### Normal user

Email: `user@example.com`
Password: `password`
