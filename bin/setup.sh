#!/bin/bash
cp .env.example .env
composer install
php artisan key:generate
php artisan make:database
php artisan migrate --seed
php artisan test
php artisan serve
