<?php

namespace Tests\Feature;

use App\Models\LoanApplication;
use App\Models\LoanRepayment;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanApplicationTest extends TestCase
{
    use DatabaseTransactions;

    public function testCustomerCanCreateLoanApplicationAndScheduledPayments()
    {
        $user = $this->createAndLoginUser('Customer');
        $response = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000
        ]);
        $response->assertCreated();
        $response->assertJsonStructure($this->jsonStructure);
        $this->assertNotNull($response->json('data.id'));
        $this->assertNotNull($response->json('data.loan_repayments'));
        $this->assertDatabaseHas('loan_applications', [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id,
            'loan_status' => 'pending',
            'approved_by' => null,
            'approved_at' => null,
            'repaid_at' => null
        ]);
        $this->assertDatabaseCount(
            LoanRepayment::where('loan_id', $response->decodeResponseJson()['data']['id'])
                ->where('payment_status', LoanRepayment::$pending)
                ->where('paid_amount', null)
                ->where('paid_at', null), 3);
    }

    public function testCustomersCannotApproveLoanApplication()
    {
        $user = $this->createAndLoginUser('Customer');
        $loanApplication = LoanApplication::create([
            'total_amount' => 5000,
            'loan_term' => 4,
            'user_id' => User::factory()->create([
                'is_admin' => 0
            ])->id
        ]);
        $response = $this->putJson(
            route('loan-applications.update', ['loan_application' => $loanApplication->id]), [
            'loan_status' => 'approved'
        ]);

        $response->assertForbidden();
        $response->assertJsonStructure($this->commonStructure);
        $response->assertJsonFragment([
            'message' => 'Customers Not allowed to access this action.',
            'success' => false
        ]);
    }

    public function testIfAdminCanApproveLoanApplication()
    {
        $user = $this->createAndLoginUser('Admin');
        $loanApplication = LoanApplication::create([
            'total_amount' => 5000,
            'loan_term' => 4,
            'user_id' => User::factory()->create([
                'is_admin' => 0
            ])->id
        ]);
        $response = $this->putJson(route('loan-applications.update', ['loan_application' => $loanApplication->id]), [
            'loan_status' => 'approved'
        ]);

        $response->assertOk();
        $response->assertJsonStructure($this->jsonStructure);
        $this->assertNotNull($response->json('data.approved_by'));
        $this->assertNotNull($response->json('data.approved_at'));
        $this->assertDatabaseHas('loan_applications', [
            'loan_term' => 4,
            'total_amount' => 5000,
            'user_id' => $loanApplication->user_id,
            'loan_status' => 'approved',
            'approved_by' => $user->id,
            'repaid_at' => null
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIfAdminCanGetAllLoanApplications(): void
    {
        $this->createAndLoginUser('Admin');
        LoanApplication::factory()->count(15)->create();
        $response = $this->get(route('loan-applications.index'));
        $response->assertOk();
        $response->assertJsonStructure($this->jsonStructure);
        $response->assertJsonCount(LoanApplication::all()->count(), 'data');
    }

    public function testIfCustomerCanViewOnlyHisLoanApplications()
    {
        $user = $this->createAndLoginUser('Customer');

        LoanApplication::create([
            'total_amount' => 5000,
            'loan_term' => 4,
            'user_id' => $user->id
        ]);

        LoanApplication::create([
            'total_amount' => 50000,
            'loan_term' => 24,
            'user_id' => User::factory()->create([
                'is_admin' => 0
            ])->id
        ]);

        $response = $this->getJson(route('loan-applications.index'));
        $response->assertOk();
        $response->assertJsonStructure($this->jsonStructure);
        $response->assertJsonCount(1, 'data');
    }
}
