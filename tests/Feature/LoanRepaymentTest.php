<?php

namespace Tests\Feature;

use App\Events\LoanRepayment\LoanEMIRepayment;
use App\Listeners\LoanRepayment\HandleLoanRepayment;
use App\Models\LoanRepayment;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class LoanRepaymentTest extends TestCase
{
    /**
     * @Desc: Loan Repayment can only be done if
     * the current scheduled payment is in pending state along with the loan application
     * @return void
     */
    public function testPayIfLoanIsNotApproved(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertStatus(400);
        $responseRepayment->assertJsonStructure($this->commonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => false,
            'message' => 'Loan repayment cannot be processed.'
        ]);
    }

    public function testPayIfScheduledEmiAlreadyPaid(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);
        $updateDetails = [
            'paid_amount' => 3333.33,
            'paid_at' => Carbon::now(),
            'payment_status' => LoanRepayment::$paid
        ];
        LoanRepayment::whereId($responseApplication->json('data.loan_repayments.0.id'))
            ->update($updateDetails);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertStatus(400);
        $responseRepayment->assertJsonStructure($this->commonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => false,
            'message' => 'Loan repayment cannot be processed.'
        ]);
    }

    public function testPayLessthanEmiAmount(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.00
        ]);
        $responseRepayment->assertStatus(400);
        $responseRepayment->assertJsonStructure($this->commonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => false,
            'message' => 'Invalid amount passed, ' .
                $responseApplication->json('data.loan_repayments.0.emi_amount') . ' is required.'
        ]);
    }

    public function testPayMoreThanTotalAmount(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertOk();

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.1.id')
        ]), [
            'paid_amount' => 4000.33
        ]);
        $responseRepayment->assertOk();

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.2.id')
        ]), [
            'paid_amount' => 3000.33
        ]);

        $responseRepayment->assertStatus(400);
        $responseRepayment->assertJsonStructure($this->commonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => false,
            'message' => 'Invalid amount passed, ' . (10000 - (3333.33 + 4000.33)) . ' is required.'
        ]);
    }

    public function testLoanRepaymentRecords(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);

        $responseRepayment->assertOk();
        $responseRepayment->assertJsonStructure($this->jsonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => true,
            'message' => 'Loan EMI Paid Successfully.'
        ]);
        $this->assertNotNull($responseRepayment->json('data.paid_at'));
        $this->assertNotNull($responseRepayment->json('data.paid_amount'));
        $this->assertDatabaseHas('loan_repayments', [
            'id' => $responseApplication->json('data.loan_repayments.0.id'),
            'loan_id' => $responseApplication->json('data.id'),
            'payment_status' => LoanRepayment::$paid
        ]);
    }

    public function testFullLoanRepaymentRecords(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertOk();

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.1.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertOk();

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.2.id')
        ]), [
            'paid_amount' => 3333.34
        ]);
        Event::fake([
            LoanEMIRepayment::class,
        ]);
        Event::assertListening(LoanEMIRepayment::class, HandleLoanRepayment::class);
        $responseRepayment->assertOk();
        $responseRepayment->assertJsonStructure($this->jsonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => true,
            'message' => 'Loan EMI Paid Successfully.'
        ]);
        $this->assertNotNull($responseRepayment->json('data.paid_at'));
        $this->assertNotNull($responseRepayment->json('data.paid_amount'));
        $this->assertDatabaseHas('loan_repayments', [
            'id' => $responseApplication->json('data.loan_repayments.2.id'),
            'loan_id' => $responseApplication->json('data.id'),
            'payment_status' => LoanRepayment::$paid
        ]);
        $this->assertNotNull($responseRepayment->json('data.loan_application.repaid_at'));
        $this->assertDatabaseHas('loan_applications', [
            'id' => $responseApplication->json('data.id'),
            'loan_status' => LoanRepayment::$paid
        ]);
    }

    public function testFullLoanRepaymentBeforeTimeRecords(): void
    {
        $user = $this->createAndLoginUser('Customer');
        $responseApplication = $this->postJson(route('loan-applications.store'), [
            'loan_term' => 3,
            'total_amount' => 10000,
            'user_id' => $user->id
        ]);
        $responseApplication->assertCreated();

        $admin = $this->createAndLoginUser('Admin');
        //Approve the loan Application
        $responseApprove = $this->putJson(route('loan-applications.update',
            ['loan_application' => $responseApplication->json('data.id')]), [
            'loan_status' => 'approved'
        ]);
        $responseApprove->assertOk();

        $this->signUserIn($user);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.0.id')
        ]), [
            'paid_amount' => 3333.33
        ]);
        $responseRepayment->assertOk();
        $this->assertDatabaseHas('loan_repayments', [
            'id' => $responseApplication->json('data.loan_repayments.0.id'),
            'loan_id' => $responseApplication->json('data.id'),
            'payment_status' => LoanRepayment::$paid
        ]);

        $responseRepayment = $this->putJson(route('loan-repayments.update', [
            'loan_repayment' => $responseApplication->json('data.loan_repayments.1.id')
        ]), [
            'paid_amount' => 6666.67
        ]);

        Event::fake([
            LoanEMIRepayment::class,
        ]);
        Event::assertListening(LoanEMIRepayment::class, HandleLoanRepayment::class);
        $responseRepayment->assertOk();
        $responseRepayment->assertJsonStructure($this->jsonStructure);
        $responseRepayment->assertJsonFragment([
            'success' => true,
            'message' => 'Loan EMI Paid Successfully.'
        ]);
        $this->assertNotNull($responseRepayment->json('data.paid_at'));
        $this->assertNotNull($responseRepayment->json('data.paid_amount'));
        $this->assertDatabaseHas('loan_repayments', [
            'id' => $responseApplication->json('data.loan_repayments.1.id'),
            'loan_id' => $responseApplication->json('data.id'),
            'payment_status' => LoanRepayment::$paid
        ]);
        $this->assertNotNull($responseRepayment->json('data.loan_application.repaid_at'));
        $this->assertDatabaseHas('loan_applications', [
            'id' => $responseApplication->json('data.id'),
            'loan_status' => LoanRepayment::$paid
        ]);
    }
}
