<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testInvalidRegister()
    {
        $response = $this->postJson(route('register'), [
            'first_name' => 'John Doe',
            'last_name' => 'John Doe',
            'email' => 'johndoe@gmail..com',
            'is_admin' => 0,
            'password' => '123456789',
        ]);
        $response->assertUnprocessable();
        $response->assertJsonFragment([
            'message' => 'The email must be a valid email address.',
        ]);

    }

    public function testCustomerRegister()
    {
        $this->registerUserAndReturnAccessTokenWithDatabaseValue(0);
    }

    public function testAdminRegister()
    {
        $this->registerUserAndReturnAccessTokenWithDatabaseValue(1);
    }

    public function testLogin()
    {
        $user = User::factory()->create();
        $response = $this->postJson(route('login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);
        $response->assertOk();
        $response->assertJsonStructure($this->jsonStructure);
        $response->assertJsonFragment([
            'message' => 'Logged In Successfully.',
            'success' => true
        ]);
        $this->assertNotNull($response->json('data.token'));
    }

    public function testFailedAuthenticationTest()
    {
        $user = User::factory()->create();
        $response = $this->postJson(route('login'), [
            'email' => $user->email,
            'password' => 'wrong_password'
        ]);
        $response->assertUnauthorized();
        $response->assertJsonStructure($this->commonStructure);
        $response->assertJsonFragment([
            'message' => 'User not authenticated.',
            'success' => false
        ]);
    }

    public function registerUserAndReturnAccessTokenWithDatabaseValue(int $userType)
    {
        $response = $this->postJson(route('register'), [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'johndoe@gmail.com',
            'is_admin' => $userType,
            'password' => '123456789',
        ]);
        $response->assertCreated();
        $response->assertJsonStructure($this->jsonStructure);
        $response->assertJsonFragment([
            'message' => ($userType == 1 ? 'Admin' : 'Customer') . ' Created.',
            'email' => 'johndoe@gmail.com'
        ]);
        $this->assertNotNull($response->json('data.token'));
        $this->assertDatabaseHas('users', [
            'email' => 'johndoe@gmail.com',
            'is_admin' => $userType,
        ]);
    }

}
