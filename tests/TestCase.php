<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Sanctum\Sanctum;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    public array $commonStructure = [
        'success',
        'message',
        'meta'
    ];

    public array $jsonStructure = [
        'success',
        'message',
        'meta',
        'data'
    ];

    public function signUserIn($user = null)
    {
        $user = $user ?: User::factory()->create();
        Sanctum::actingAs($user);
        return $user;
    }

    public function createAndLoginUser(string $userType): User
    {
        $user = User::factory()->create([
            'is_admin' => $userType == 'Customer' ? 0 : 1
        ]);
        $this->actingAs($user);
        return $user;
    }
}
