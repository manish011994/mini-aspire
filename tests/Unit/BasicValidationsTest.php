<?php

namespace Tests\Unit;

use App\Actions\LoanApplication\LoanApplicationAction;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use PHPUnit\Framework\TestCase;

class BasicValidationsTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A test for EMI Dates
     *
     * @return void
     */
    public function testEmiDates(): void
    {
        $loanApplication = new LoanApplicationAction();
        $week = rand(1, 24);
        $result = $loanApplication->getEMIDates($week);
        $this->assertCount($week, $result);
        $this->assertIsArray($result);
    }

    /**
     * To test if the total amount and EMIs match when scheduled
     * @return void
     */
    public function testEmiAmounts(): void
    {
        $week = rand(1, 24);
        $totalAmount = rand(1000, 100000);
        $loanApplication = new LoanApplicationAction();
        $resultAmounts = $loanApplication->getEMIAmounts($totalAmount, $week);
        $this->assertIsArray($resultAmounts);
        $this->assertCount($week, $resultAmounts);
        $this->assertEquals($totalAmount, array_sum($resultAmounts));
    }
}
