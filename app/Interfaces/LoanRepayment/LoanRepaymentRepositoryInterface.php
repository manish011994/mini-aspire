<?php

namespace App\Interfaces\LoanRepayment;

interface LoanRepaymentRepositoryInterface
{
    public function getAllLoanRepayments();
    public function getAllLoanRepaymentsOfCustomer(int $customerId);
    public function getLoanRepaymentById($loanRepaymentId);
    public function updateLoanRepayment($loanRepaymentId, array $updatedDetails);
    public function getEMIAmount($loanRepaymentId);
    public function getMinimumEMIAmount($loanRepaymentDetails);
    public function getTotalEMIPaid($loanRepaymentDetails);
    public function getTotalLoanAmount($loanRepaymentDetails);
    public function getRemainingLoanAmount($loanRepaymentDetails);
    public function isLastEMI($loanRepaymentDetails);
    public function getPendingEMIs($loanRepaymentDetails);
}
