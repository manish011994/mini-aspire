<?php

namespace App\Interfaces\LoanApplication;

interface LoanApplicationRepositoryInterface
{
    public function getAllLoanApplications();
    public function getAllLoanApplicationsOfCustomer(int $customerId);
    public function getLoanApplicationById($loanApplicationId);
    public function updateLoanApplication($loanApplicationId, array $updatedDetails);
}
