<?php

namespace App\Events\LoanRepayment;

use App\Models\LoanRepayment;
use App\Repositories\LoanApplication\LoanApplicationRepository;
use App\Repositories\LoanRepayment\LoanRepaymentRepository;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LoanEMIRepayment
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public LoanRepayment $loanRepayment;
    public LoanApplicationRepository $loanApplicationRepository;
    public LoanRepaymentRepository $loanRepaymentRepository;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        LoanRepayment $loanRepayment,
        LoanApplicationRepository $loanApplicationRepository,
        LoanRepaymentRepository $loanRepaymentRepository)
    {
        $this->loanRepayment = $loanRepayment;
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanRepaymentRepository = $loanRepaymentRepository;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
