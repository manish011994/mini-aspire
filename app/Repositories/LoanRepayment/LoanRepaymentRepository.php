<?php

namespace App\Repositories\LoanRepayment;

use App\Interfaces\LoanRepayment\LoanRepaymentRepositoryInterface;
use App\Models\LoanRepayment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class LoanRepaymentRepository implements LoanRepaymentRepositoryInterface
{

    public function getAllLoanRepayments(): Collection|array
    {
        return LoanRepayment::with('loanApplication')->get();
    }

    public function getAllLoanRepaymentsOfCustomer(int $customerId): Model|Collection|Builder|array|null
    {
        return LoanRepayment::with('loanApplication')->where('user_id', $customerId)->get();
    }

    public function getLoanRepaymentById($loanRepaymentId): Model|Collection|Builder|array|null
    {
        return LoanRepayment::with('loanApplication')->findOrFail($loanRepaymentId);
    }

    public function updateLoanRepayment($loanRepaymentId, array $updatedDetails)
    {
        return LoanRepayment::whereId($loanRepaymentId)->update($updatedDetails);
    }

    public function getMinimumEMIAmount($loanRepaymentDetails): float
    {
        return (double)$loanRepaymentDetails->loanApplicationId()->min('emi_amount');
    }

    public function getTotalEMIPaid($loanRepaymentDetails): float
    {
        return (double)$loanRepaymentDetails->loanApplicationId()->sum('paid_amount');
    }

    public function getTotalLoanAmount($loanRepaymentDetails): float
    {
        return $loanRepaymentDetails->loanApplication->total_amount;
    }

    public function getRemainingLoanAmount($loanRepaymentDetails): float
    {
        return $this->getTotalLoanAmount($loanRepaymentDetails) - $this->getTotalEMIPaid($loanRepaymentDetails);
    }

    public function isLastEMI($loanRepaymentDetails): bool
    {
        $lastEMIId = $loanRepaymentDetails->loanApplication->loanRepayments->sortByDesc('id')->first()->id;
        return $loanRepaymentDetails->id == $lastEMIId;
    }

    public function getEMIAmount($loanRepaymentId): float
    {
        $loanRepaymentDetails = $this->getLoanRepaymentById($loanRepaymentId);
        $remainingAmount = $this->getRemainingLoanAmount($loanRepaymentDetails);
        if (($remainingAmount == $loanRepaymentDetails->emi_amount) ||
            $remainingAmount < $loanRepaymentDetails->emi_amount) {
            $amount = $remainingAmount;
        } else {
            $amount = $this->getMinimumEMIAmount($loanRepaymentDetails);
        }
        return (double)number_format($amount, 2, ".", "");
    }

    public function checkIfAmountCanBePaid($paidAmount, $loanRepaymentDetails): bool
    {
        return $this->getRemainingLoanAmount($loanRepaymentDetails) >= $paidAmount;
    }

    public function getPendingEMIs($loanRepaymentDetails)
    {
        return $loanRepaymentDetails->loanApplication->loanRepayments->where('payment_status', LoanRepayment::$pending);
    }
}
