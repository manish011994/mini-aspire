<?php

namespace App\Repositories\LoanApplication;

use App\Interfaces\LoanApplication\LoanApplicationRepositoryInterface;
use App\Models\LoanApplication;
use Illuminate\Database\Eloquent\Collection;

class LoanApplicationRepository implements LoanApplicationRepositoryInterface
{

    public function getAllLoanApplications(): Collection
    {
        return LoanApplication::with('loanRepayments')->get();
    }

    public function getLoanApplicationById($loanApplicationId): \Illuminate\Database\Eloquent\Builder|array|Collection|\Illuminate\Database\Eloquent\Model
    {
        return LoanApplication::with('loanRepayments')->findOrFail($loanApplicationId);
    }

    public function updateLoanApplication($loanApplicationId, array $updatedDetails): bool
    {
        return LoanApplication::whereId($loanApplicationId)->update($updatedDetails);
    }

    public function getAllLoanApplicationsOfCustomer(int $customerId): Collection
    {
        return LoanApplication::with('loanRepayments')->where('user_id', $customerId)->get();
    }
}
