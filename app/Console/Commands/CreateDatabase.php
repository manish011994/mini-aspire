<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PDO;
use PDOException;

class CreateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will create the database for the application';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $db = env('DB_DATABASE');
        try {
            $pdo = $this->getPDOConnection(env('DB_HOST'), env('DB_PORT'), env('DB_USERNAME'), env('DB_PASSWORD'));

            $pdo->exec(sprintf(
                'CREATE DATABASE IF NOT EXISTS %s;',
                $db
            ));
            $this->info(sprintf('Successfully created %s database', $db));
        } catch (PDOException $exception) {
            $this->error("Failed to create Database. ". $exception->getMessage());
        }
    }

    private function getPDOConnection($host, $port, $username, $password): PDO
    {
        return new PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }
}
