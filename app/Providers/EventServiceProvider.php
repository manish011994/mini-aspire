<?php

namespace App\Providers;

use App\Events\LoanRepayment\LoanEMIRepayment;
use App\Listeners\LoanRepayment\HandleLoanRepayment;
use App\Models\LoanApplication;
use App\Models\LoanRepayment;
use App\Observers\LoanApplication\LoanApplicationObserver;
use App\Observers\LoanRepayment\LoanRepaymentObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        LoanEMIRepayment::class => [
            HandleLoanRepayment::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        LoanApplication::observe(LoanApplicationObserver::class);
        LoanRepayment::observe(LoanRepaymentObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
