<?php

namespace App\Providers;

use App\Interfaces\LoanApplication\LoanApplicationRepositoryInterface;
use App\Interfaces\LoanRepayment\LoanRepaymentRepositoryInterface;
use App\Repositories\LoanApplication\LoanApplicationRepository;
use App\Repositories\LoanRepayment\LoanRepaymentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(LoanApplicationRepositoryInterface::class, LoanApplicationRepository::class);
        $this->app->bind(LoanRepaymentRepositoryInterface::class, LoanRepaymentRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
