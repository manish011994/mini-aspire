<?php

namespace App\Observers\LoanApplication;

use App\Models\LoanApplication;

class LoanApplicationObserver
{
    /**
     * @param LoanApplication $loanApplication
     * @return void
     */
    public function creating(LoanApplication $loanApplication):void
    {
        $loanApplication->loan_unique_id = uniqid('AL');
    }
    /**
     * Handle the LoanApplication "created" event.
     *
     * @param  \App\Models\LoanApplication  $loanApplication
     * @return void
     */
    public function created(LoanApplication $loanApplication)
    {

    }

    /**
     * Handle the LoanApplication "updated" event.
     *
     * @param  \App\Models\LoanApplication  $loanApplication
     * @return void
     */
    public function updated(LoanApplication $loanApplication)
    {
        //
    }

    /**
     * Handle the LoanApplication "deleted" event.
     *
     * @param  \App\Models\LoanApplication  $loanApplication
     * @return void
     */
    public function deleted(LoanApplication $loanApplication)
    {
        //
    }

    /**
     * Handle the LoanApplication "restored" event.
     *
     * @param  \App\Models\LoanApplication  $loanApplication
     * @return void
     */
    public function restored(LoanApplication $loanApplication)
    {
        //
    }

    /**
     * Handle the LoanApplication "force deleted" event.
     *
     * @param  \App\Models\LoanApplication  $loanApplication
     * @return void
     */
    public function forceDeleted(LoanApplication $loanApplication)
    {
        //
    }
}
