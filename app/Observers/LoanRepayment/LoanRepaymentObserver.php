<?php

namespace App\Observers\LoanRepayment;

use App\Models\LoanRepayment;

class LoanRepaymentObserver
{
    /**
     * Handle the LoanRepayment "created" event.
     *
     * @param  \App\Models\LoanRepayment  $loanRepayment
     * @return void
     */
    public function created(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "updated" event.
     *
     * @param  \App\Models\LoanRepayment  $loanRepayment
     * @return void
     */
    public function updated(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "deleted" event.
     *
     * @param  \App\Models\LoanRepayment  $loanRepayment
     * @return void
     */
    public function deleted(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "restored" event.
     *
     * @param  \App\Models\LoanRepayment  $loanRepayment
     * @return void
     */
    public function restored(LoanRepayment $loanRepayment)
    {
        //
    }

    /**
     * Handle the LoanRepayment "force deleted" event.
     *
     * @param  \App\Models\LoanRepayment  $loanRepayment
     * @return void
     */
    public function forceDeleted(LoanRepayment $loanRepayment)
    {
        //
    }
}
