<?php

namespace App\Actions\LoanRepayment;

use App\Events\LoanRepayment\LoanEMIRepayment;
use App\Http\Resources\LoanRepayment\LoanRepaymentResource;
use App\Http\Resources\LoanRepayment\LoanRepaymentWithApplication;
use App\Models\LoanRepayment;
use App\Repositories\LoanApplication\LoanApplicationRepository;
use App\Repositories\LoanRepayment\LoanRepaymentRepository;
use App\Traits\ApiResponder;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class LoanRepaymentAction
{
    use ApiResponder;

    protected LoanRepayment $loanRepayment;
    protected LoanApplicationRepository $loanApplicationRepository;
    protected LoanRepaymentRepository $loanRepaymentRepository;
    private array $loanRepaymentData;

    public function __construct(
        LoanApplicationRepository $loanApplicationRepository,
        LoanRepaymentRepository   $loanRepaymentRepository,
        LoanRepayment             $loanRepayment)
    {
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanRepaymentRepository = $loanRepaymentRepository;
        $this->loanRepayment = $loanRepayment;
    }

    public function handleRepayment($repaymentRequest): JsonResponse
    {
        $this->assignLoanRepaymentData($repaymentRequest);
        if (!$this->isEMIValid()) {
            return $this->sendError('Loan repayment cannot be processed.', [], 400);
        }
        $this->checkEMIAmountValid();
        $this->updateLoanRepayment();
        $this->triggerLoanEMIRepayment();
        return $this->sendSuccess(
            new LoanRepaymentWithApplication($this->loanRepaymentRepository->getLoanRepaymentById($this->loanRepayment->id)),
            'Loan EMI Paid Successfully.');
    }

    private function assignLoanRepaymentData($repaymentData): void
    {
        $this->loanRepaymentData = $repaymentData;
    }

    private function isEMIValid(): bool
    {
        return (($this->loanRepayment->loanApplication->isLoanPayable()) &&
            ($this->loanRepayment->payment_status == LoanRepayment::$pending));
    }

    private function checkEMIAmountValid(): bool
    {
        $emiAmountRequired = $this->loanRepaymentRepository->getEMIAmount($this->loanRepayment->id);
        if ($emiAmountRequired < $this->loanRepaymentData['paid_amount']) {
            $validAmount = $this->loanRepaymentRepository->checkIfAmountCanBePaid($this->loanRepaymentData['paid_amount'], $this->loanRepayment);
            if (!$validAmount) {
                abort($this->sendError('Invalid amount passed, ' . $emiAmountRequired . ' is required.', [], 400));
            }
        } else if ($emiAmountRequired > $this->loanRepaymentData['paid_amount']) {
            abort($this->sendError('Invalid amount passed, ' . $emiAmountRequired . ' is required.', [], 400));
        }
        return true;
    }

    private function updateLoanRepayment(): void
    {
        $updateDetails = $this->loanRepaymentData;
        $updateDetails['paid_at'] = Carbon::now();
        $updateDetails['payment_status'] = LoanRepayment::$paid;
        $this->loanRepaymentRepository
            ->updateLoanRepayment($this->loanRepayment->id, $updateDetails);
        $this->loanRepayment = $this->loanRepaymentRepository->getLoanRepaymentById($this->loanRepayment->id);
    }

    private function triggerLoanEMIRepayment(): void
    {
        event(new LoanEMIRepayment($this->loanRepayment, $this->loanApplicationRepository, $this->loanRepaymentRepository));
    }
}
