<?php

namespace App\Actions\LoanApplication;

use App\Models\LoanApplication;
use App\Models\LoanRepayment;
use App\Traits\ApiResponder;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class LoanApplicationAction
{
    use ApiResponder;

    protected LoanApplication $loanApplication;
    protected array $loanApplicationData;

    public function handle($loanApplicationRequest): JsonResponse
    {
        $this->assignLoanApplicationData($loanApplicationRequest);
        $this->createLoanApplication();
        $this->scheduleRepayments();
        return $this->sendSuccess($this->loanApplication, 'Loan Application created.', 201);
    }

    public function assignLoanApplicationData($loanApplicationRequest): void
    {
        $this->loanApplicationData = $loanApplicationRequest;
    }

    private function createLoanApplication(): LoanApplication
    {
        $this->loanApplication = LoanApplication::create([
            'loan_term' => $this->loanApplicationData['loan_term'],
            'total_amount' => $this->loanApplicationData['total_amount'],
            'user_id' => auth()->user()->getAuthIdentifier()
        ]);
        $this->loanApplicationData['loanData'] = $this->loanApplication;
        return $this->loanApplication;
    }

    private function scheduleRepayments(): void
    {
        $term = $this->loanApplicationData['loan_term'];
        $totalAmount = $this->loanApplicationData['total_amount'];
        $emiDates = $this->getEMIDates($term);
        $emiAmounts = $this->getEMIAmounts($totalAmount, $term);

        for ($i = 0; $i < $term; $i++) {
            LoanRepayment::create([
                'loan_id' => $this->loanApplication->id,
                'emi_date' => $emiDates[$i],
                'emi_amount' => $emiAmounts[$i]
            ]);
        }
        $this->loanApplication->loan_repayments = $this->loanApplication->loanRepayments;
    }

    public function getEMIDates($weeks): array
    {
        $emiDates = [];
        $currentDate = Carbon::now();
        for ($i = 1; $i <= $weeks; $i++) {
            $currentDate->addWeek();
            $emiDates[] = $currentDate->format('Y-m-d');
        }
        return $emiDates;
    }

    public function getEMIAmounts($totalAmount, $weeks): array
    {
        $emiAmounts = [];
        $emi = number_format(($totalAmount / $weeks), 2, '.', '');
        for ($i = 1; $i <= $weeks; $i++) {
            $emiAmounts[] = $emi;
        }

        // Modify last EMI if differences
        $lastEMIKey = $weeks - 1;
        if (array_sum($emiAmounts) < $totalAmount) {
            $emiAmounts[$lastEMIKey] = $emiAmounts[$lastEMIKey] + ($totalAmount - array_sum($emiAmounts));
        } else if (array_sum($emiAmounts) > $totalAmount) {
            $emiAmounts[$lastEMIKey] = $emiAmounts[$lastEMIKey] - (array_sum($emiAmounts) - $totalAmount);
        }

        return $emiAmounts;
    }
}
