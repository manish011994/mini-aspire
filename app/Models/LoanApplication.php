<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class LoanApplication extends Model
{
    use HasFactory;
    public static string $approved = 'approved';
    public static string $paid = 'paid';

    protected $fillable = [
        'loan_term',
        'total_amount',
        'user_id'
    ];

    protected $casts = [
        'approved_at',
        'repaid_at'
    ];

    /**
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function loanRepayments(): HasMany
    {
        return $this->hasMany(LoanRepayment::class, "loan_id");
    }

    public function loanApprover(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'approved_by');
    }

    public function isLoanPayable(): bool
    {
        return $this->repaid_at == null && $this->loan_status == static::$approved;
    }
}
