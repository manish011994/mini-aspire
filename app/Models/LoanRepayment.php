<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LoanRepayment extends Model
{
    use HasFactory;
    public static string $paid = "paid";
    public static string $pending = "pending";

    protected $fillable = [
        'loan_id',
        'emi_date',
        'emi_amount',
        'user_id',
    ];

    protected $casts = [
        'emi_date',
        'paid_at'
    ];

    /**
     * @return BelongsTo
     */
    public function loanApplication(): BelongsTo
    {
        return $this->belongsTo(LoanApplication::class, "loan_id");
    }

    public function scopeLoanApplicationId()
    {
        return $this->where('loan_id', $this->loanApplication->id);
    }

}
