<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Parental\HasParent;

class Customer extends User
{
    use HasFactory, HasParent;

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(function ($query) {
            $query->where('is_admin', 0);
        });
    }

    public function loanApplications(): HasMany
    {
        return $this->hasMany(LoanApplication::class, 'user_id');
    }
}
