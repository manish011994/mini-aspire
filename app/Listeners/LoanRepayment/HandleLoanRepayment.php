<?php

namespace App\Listeners\LoanRepayment;

use App\Events\LoanRepayment\LoanEMIRepayment;
use App\Models\LoanApplication;
use App\Models\LoanRepayment;
use App\Repositories\LoanRepayment\LoanRepaymentRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class HandleLoanRepayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoanEMIRepayment $event
     * @return void
     */
    public function handle(LoanEMIRepayment $event)
    {
        $remainingAmount = $event->loanRepaymentRepository->getRemainingLoanAmount($event->loanRepayment);
        $isLastEMI = $event->loanRepaymentRepository->isLastEMI($event->loanRepayment);
        if ($remainingAmount == 0 || $isLastEMI) {
            // Update Loan Status
            $updateDetails = [
                'loan_status' => LoanApplication::$paid,
                'repaid_at' => Carbon::now()->format('Y-m-d H:i:s')
            ];
            $event->loanApplicationRepository->updateLoanApplication(
                $event->loanRepayment->loan_id,
                $updateDetails);
            // Update Loan Repayment status
            $pendingEMIs = $event->loanRepaymentRepository->getPendingEMIs($event->loanRepayment);
            foreach ($pendingEMIs as $emi) {
                $updateDetails = [
                    'paid_amount' => 0,
                    'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'payment_status' => LoanRepayment::$paid
                ];
                $event->loanRepaymentRepository->updateLoanRepayment($emi->id, $updateDetails);
            }
        }
    }
}
