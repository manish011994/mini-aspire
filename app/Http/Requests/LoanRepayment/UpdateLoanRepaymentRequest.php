<?php

namespace App\Http\Requests\LoanRepayment;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLoanRepaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'paid_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
        ];
    }
}
