<?php

namespace App\Http\Requests\LoanApplication;

use App\Traits\ApiResponder;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLoanApplicationStatusRequest extends FormRequest
{
    use ApiResponder;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'loan_status' => 'required|in:approved,rejected,cancelled'
        ];
    }

    protected function failedAuthorization()
    {
        abort($this->sendError('Customers Not allowed to access this action.', [], 403));
    }
}
