<?php

namespace App\Http\Requests\LoanApplication;

use App\Traits\ApiResponder;
use Illuminate\Foundation\Http\FormRequest;

class CreateLoanApplicationRequest extends FormRequest
{
    use ApiResponder;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->user()->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'loan_term' => 'required|integer|min:1|max:520',
            'total_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/'
        ];
    }

    protected function failedAuthorization()
    {
        abort($this->sendError('Admins Not allowed to access this action.', [], 403));
    }
}
