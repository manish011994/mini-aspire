<?php

namespace App\Http\Resources\LoanRepayment;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanRepaymentWithApplication extends LoanRepaymentResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array|\JsonSerializable|\Illuminate\Contracts\Support\Arrayable
    {
        $response = parent::toArray($request);
        $response['loan_application'] = $this->loanApplication;
        return $response;
    }
}
