<?php

namespace App\Http\Resources\LoanRepayment;

use App\Http\Resources\LoanApplication\LoanApplicationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanRepaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'loan_id' => $this->loan_id,
            'emi_date' => $this->emi_date,
            'emi_amount' => $this->emi_amount,
            'paid_amount' => $this->paid_amount,
            'paid_at' => $this->paid_at,
            'payment_status' => $this->payment_status,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
