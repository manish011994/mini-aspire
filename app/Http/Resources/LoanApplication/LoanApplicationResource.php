<?php

namespace App\Http\Resources\LoanApplication;

use App\Http\Resources\LoanRepayment\LoanRepaymentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "loan_unique_id" => $this->loan_unique_id,
            "loan_term" => $this->loan_term,
            "loan_status" => $this->loan_status,
            "total_amount" => $this->total_amount,
            "user_id" => $this->user_id,
            "approved_by" => $this->when($request->user()->isAdmin(), $this->approved_by),
            "approved_at" => $this->when($request->user()->isAdmin(), $this->approved_at),
            "repaid_at" => $this->repaid_at,
            "created_at" => $this->created_at->format('Y-m-d H:i:s'),
            "updated_at" => $this->updated_at->format('Y-m-d H:i:s'),
            'loan_repayment_details' => LoanRepaymentResource::collection($this->loanRepayments),
        ];
    }
}
