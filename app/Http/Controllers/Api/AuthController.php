<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponder;

    public function register(RegisterRequest $request): JsonResponse
    {
        $request = $request->all();
        $request['password'] = bcrypt($request['password']);
        $user = User::create($request);

        $user->token = $user->createToken('SecretToken')->plainTextToken;
        $userType = ($request['is_admin']) ? 'Admin' : 'Customer';

        return $this->sendSuccess($user, $userType . ' Created.', 201);
    }

    public function login(LoginRequest $request): JsonResponse
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $user->token = $user->createToken('SecretToken')->plainTextToken;
            return $this->sendSuccess($user, 'Logged In Successfully.');
        } else {
            return $this->sendError('User not authenticated.', [], 401);
        }
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();
        return $this->sendSuccess([], 'Logged out successfully.');
    }
}
