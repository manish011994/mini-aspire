<?php

namespace App\Http\Controllers\Api;

use App\Actions\LoanRepayment\LoanRepaymentAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoanRepayment\UpdateLoanRepaymentRequest;
use App\Http\Resources\LoanRepayment\LoanRepaymentResource;
use App\Models\LoanRepayment;
use App\Repositories\LoanApplication\LoanApplicationRepository;
use App\Repositories\LoanRepayment\LoanRepaymentRepository;
use App\Traits\ApiResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoanRepaymentController extends Controller
{
    use ApiResponder;

    protected LoanApplicationRepository $loanApplicationRepository;
    private LoanRepaymentRepository $loanRepaymentRepository;

    public function __construct(LoanApplicationRepository $loanApplicationRepository, LoanRepaymentRepository $repaymentRepository)
    {
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanRepaymentRepository = $repaymentRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(LoanRepayment $loanRepayment): JsonResponse
    {
        $loanRepaymentDetail = $this->loanRepaymentRepository->getLoanRepaymentById($loanRepayment->id);
        return $this->sendSuccess(new LoanRepaymentResource($loanRepaymentDetail));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLoanRepaymentRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateLoanRepaymentRequest $request, LoanRepayment $loanRepayment):JsonResponse
    {
        $loanRepaymentAction = new LoanRepaymentAction(
            $this->loanApplicationRepository,
            $this->loanRepaymentRepository,
            $loanRepayment);
        $requestData = $request->only([
            'paid_amount'
        ]);
        return $loanRepaymentAction->handleRepayment($requestData);
    }
}
