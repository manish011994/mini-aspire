<?php

namespace App\Http\Controllers\Api;

use App\Actions\LoanApplication\LoanApplicationAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoanApplication\CreateLoanApplicationRequest;
use App\Http\Requests\LoanApplication\UpdateLoanApplicationStatusRequest;
use App\Http\Resources\LoanApplication\LoanApplicationResource;
use App\Interfaces\LoanApplication\LoanApplicationRepositoryInterface;
use App\Models\LoanApplication;
use App\Traits\ApiResponder;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoanApplicationController extends Controller
{
    use ApiResponder;

    private LoanApplicationRepositoryInterface $loanApplicationRepository;

    public function __construct(LoanApplicationRepositoryInterface $loanApplicationRepository)
    {
        $this->loanApplicationRepository = $loanApplicationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if ($request->user()->isAdmin()) {
            $customerLoanApplications = $this->loanApplicationRepository->getAllLoanApplications();
        } else {
            $customerLoanApplications = $this->loanApplicationRepository->getAllLoanApplicationsOfCustomer($request->user()->id);
        }
        return $this->sendSuccess(LoanApplicationResource::collection($customerLoanApplications));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateLoanApplicationRequest $request
     * @return void
     */
    public function store(CreateLoanApplicationRequest $request): JsonResponse
    {
        $action = new LoanApplicationAction();
        return $action->handle($request->validated());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(LoanApplication $loanApplication): JsonResponse
    {
        $this->authorize('view', $loanApplication);
        $loanApplicationDetail = $this->loanApplicationRepository->getLoanApplicationById($loanApplication->id);
        return $this->sendSuccess(new LoanApplicationResource($loanApplicationDetail));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLoanApplicationStatusRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLoanApplicationStatusRequest $request, LoanApplication $loanApplication): JsonResponse
    {
        $updateDetails = $request->only(['loan_status']);
        $updateDetails['approved_by'] = auth()->user()->getAuthIdentifier();
        $updateDetails['approved_at'] = Carbon::now()->format('Y-m-d H:i:s');
        $this->loanApplicationRepository->updateLoanApplication($loanApplication->id, $updateDetails);
        $loanApplicationDetail = $this->loanApplicationRepository->getLoanApplicationById($loanApplication->id);
        return $this->sendSuccess(new LoanApplicationResource($loanApplicationDetail), 'Loan Application status updated successfully.');
    }

}
